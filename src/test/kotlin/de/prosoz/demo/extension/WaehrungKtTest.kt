package de.prosoz.demo.extension

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class WaehrungKtTest {

    class ExtensionTest {
        @Test
        fun euro() {
            val e = 10.euro
            assertThat(e).isEqualTo(Euro(10))
        }

    }
}