package de.prosoz.demo.fraction

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class CollectionTest {

    @Test
    fun `Teste die Summe von Quadratzahlen`() {
        val nums = listOf(1, 2, 3, 4, 5)
        
        val sumOfSquares = nums.filter { it > 2 }.map { it * it }.sum()

        assertThat(sumOfSquares).isEqualTo(50)
    }

}