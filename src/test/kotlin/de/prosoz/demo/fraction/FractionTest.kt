package de.prosoz.demo.fraction

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class FractionTest {
    class ConstructorTests {
        @Test
        fun `create a new fraction`() {
            val fraction = Fraction(1, 2)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(1)
            assertThat(denominator).isEqualTo(2)
        }

        @Test
        fun `create a new large fraction`() {
            val fraction = Fraction(Long.MAX_VALUE - 1, Long.MAX_VALUE)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(Long.MAX_VALUE - 1)
            assertThat(denominator).isEqualTo(Long.MAX_VALUE)
        }

        @Test
        fun `create a fraction with denominator 1`() {
            val fraction = Fraction(10, 1)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(10)
            assertThat(denominator).isEqualTo(1)
        }

        @Test
        fun `fail to create a fraction with denominator 0`() {
            assertThatThrownBy { Fraction(1, 0) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessageContaining("Denominator is 0")
        }

        @Test
        fun `Create a fraction out of a number`() {
            val fraction = Fraction(13)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(13)
            assertThat(denominator).isEqualTo(1)
        }
    }

    class RecuceFractionTests {
        @Test
        fun `simple reduction`() {
            val fraction = Fraction(2, 6)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(1)
            assertThat(denominator).isEqualTo(3)
        }

        @ParameterizedTest
        @CsvSource(
            "2, 4, 1, 2",
            "18, 24, 3, 4",
            "85, 65, 17, 13",
            "-2, 4, -1, 2",
            "3, -9, -1, 3",
            "-15, -25, 3, 5"
        )
        fun `complicated reductions`(
            numeratorGiven: Long,
            denominatorGiven: Long,
            numeratorExpected: Long,
            denominatorExpected: Long
        ) {
            val fraction = Fraction(numeratorGiven, denominatorGiven)

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(numeratorExpected)
            assertThat(denominator).isEqualTo(denominatorExpected)
        }

    }

    class EqualsTests {
        @ParameterizedTest
        @CsvSource(
            "1, 3, 1, 3",
            "2, 6, 1, 3",
            "-1, 3, 1, -3"
        )
        fun `compare equal fractions`(numerator1: Int, denominator1: Int, numerator2: Int, denominator2: Int) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)

            assertThat(f1).isEqualTo(f2)
        }

        @Test
        fun `compare identical fractions`() {
            val f1 = Fraction(1, 4)
            val f2: Fraction = f1

            assertThat(f1).isEqualTo(f2)
        }

        @Test
        fun `compare a fraction with null`() {
            val f1: Fraction? = Fraction(1, 5)

            val result = f1 == null

            assertThat(result).isFalse()
        }

        @ParameterizedTest
        @CsvSource(
            "1, 3, 1, 4",
            "2, 7, 1, 3",
            "-1, 3, 1, 3"
        )
        fun `compare fractions that are not equal`(
            numerator1: Int,
            denominator1: Int,
            numerator2: Int,
            denominator2: Int
        ) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)

            assertThat(f1).isNotEqualTo(f2)
        }

    }

    class HashTests {
        @Test
        fun `Identical franctions produce identical hashCodes`() {
            val f1 = Fraction(2, 3)
            val f2 = Fraction(2, 3)

            assertThat(f1.hashCode()).isEqualTo(f2.hashCode())
        }

        @Test
        fun `Different fractions produce normally different hashCodes`() {
            val f1 = Fraction(3, 4)
            val f2 = Fraction(4, 5)

            assertThat(f1.hashCode()).isNotEqualTo(f2.hashCode())
        }
    }

    class ToStringTests {
        @ParameterizedTest
        @CsvSource(
            "1, 3, 1 / 3",
            "14, 15, 14 / 15",
            "-3, 4, -3 / 4",
            "3, -4, -3 / 4",
            "15, 1, 15",
            "0, 3, 0"
        )
        fun `toString of a fraction`(numerator: Int, denominator: Int, result: String) {
            val fraction = Fraction(numerator, denominator)

            assertThat(fraction.toString()).isEqualTo(result)
        }

    }

    class AddTests {
        @Test
        fun `add 2 fractions with common denominator`() {
            val f1 = Fraction(2, 9)
            val f2 = Fraction(5, 9)

            val result = f1 + f2

            assertThat(result).isEqualTo(Fraction(7, 9))
        }

        @Test
        fun `add 2 fractions with a common gcd`() {
            val f1 = Fraction(3, 20)
            val f2 = Fraction(5, 12)

            val result = f1 + f2

            assertThat(result).isEqualTo(Fraction(17, 30))
        }

        @Test
        fun `add 2 fractions with prime denominators`() {
            val f1 = Fraction(2, 7)
            val f2 = Fraction(5, 11)

            val result = f1 + f2

            assertThat(result).isEqualTo(Fraction(57, 77))
        }

        @Test
        fun `add 2 fractions where one has a negativ sign`() {
            val f1 = Fraction(-2, 9)
            val f2 = Fraction(2, 3)

            val result = f1 + f2

            assertThat(result).isEqualTo(Fraction(4, 9))
        }

    }

    class SubtractTests {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 1, 3, 1, 6",
            "15, 1, 13, 1, 2, 1",
            "3, 5, -2, 5, 1, 1"
        )
        fun `subtract 2 fractions`(
            numerator1: Int,
            denominator1: Int,
            numerator2: Int,
            denominator2: Int,
            numeratorResult: Int,
            denominatorResult: Int
        ) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)
            val result = Fraction(numeratorResult, denominatorResult)

            assertThat(f1 - f2).isEqualTo(result)
        }
    }

    class MultiplyTests {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 1, 3, 1, 6",
            "3, 10, 5, 9, 1, 6",
            "3, 5, -2, 5, -6, 25",
            "0, 3, 4, 5, 0, 15",
            "2, 1, 3, 1, 6, 1"
        )
        fun `multiply 2 fractions`(
            numerator1: Int,
            denominator1: Int,
            numerator2: Int,
            denominator2: Int,
            numeratorResult: Int,
            denominatorResult: Int
        ) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)
            val result = Fraction(numeratorResult, denominatorResult)

            assertThat(f1 * f2).isEqualTo(result)
        }
    }

    class DivisionTests {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 1, 3, 3, 2",
            "4, 5, 4, 5, 1, 1",
            "1, 6, 1, 5, 5, 6",
            "0, 3, 1, 4, 0, 3",
            "-1, 4, -1, 2, 1, 2"
        )
        fun `divide 2 fractions`(
            numerator1: Int,
            denominator1: Int,
            numerator2: Int,
            denominator2: Int,
            numeratorResult: Int,
            denominatorResult: Int
        ) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)
            val result = Fraction(numeratorResult, denominatorResult)

            assertThat(f1 / f2).isEqualTo(result)
        }

        @Test
        fun `divide by 0`() {
            val f1 = Fraction(1, 2)
            val f2 = Fraction(0, 3)

            assertThatThrownBy { f1 / f2 }
                .isInstanceOf(IllegalArgumentException::class.java)
        }
    }

    class ParseTests {
        @ParameterizedTest
        @CsvSource(
            "1/5, 1, 5",
            "15 / 20, 3, 4",
            "3 / 1, 3, 1",
            "3, 3, 1",
            "-2 / 3, -2, 3",
            "0 / 1, 0, 1"
        )
        fun `parse regular fractions or throw exception`(
            text: String,
            numeratorExpected: Long,
            denominatorExpected: Long
        ) {
            val fraction = text.toFraction()

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(numeratorExpected)
            assertThat(denominator).isEqualTo(denominatorExpected)
        }

        @ParameterizedTest
        @CsvSource(
            "/ 1",
            "10 / 0",
            "1./.3"
        )
        fun `parse wrong expressions and throw exception`(text: String) {
            assertThatThrownBy { text.toFraction() }
                .isInstanceOf(IllegalArgumentException::class.java)
        }

        @ParameterizedTest
        @CsvSource(
            "1/4, 1, 4",
            "24 / 40, 3, 5",
            "13 / 1, 13, 1",
            "8, 8, 1",
            "-4 / 3, -4, 3",
            "0 / 5, 0, 1"
        )
        fun `parse regular fractions or return null`(text: String, numeratorExpected: Long, denominatorExpected: Long) {
            val fraction = text.toFraction()

            val (numerator, denominator) = fraction

            assertThat(numerator).isEqualTo(numeratorExpected)
            assertThat(denominator).isEqualTo(denominatorExpected)
        }

        @ParameterizedTest
        @CsvSource(
            "/ 1",
            "10 / 0",
            "1./.3",
            "1 / 2 / 3"
        )
        fun `parse wrong expressions and return null`(text: String) {
            assertThat(text.toFractionOrNull()).isNull()
        }

        @Test
        fun `parse null string`() {
            val text: String? = null

            val result = text.toFractionOrNull()

            assertThat(result).isNull()
        }

    }

    class ConversionTests {
        @Test
        fun `convert int to fraction`() {
            assertThat(7.toFraction()).isEqualTo(Fraction(7, 1))
        }

        @Test
        fun `convert long to fraction`() {
            assertThat(123456L.toFraction()).isEqualTo(Fraction(123456L, 1L))
        }

    }

    class CompareTests {
        @ParameterizedTest
        @CsvSource(
            "1, 2, 2, 3",
            "0, 3, 1, 3",
            "-2, 3, 1, 5"
        )
        fun smaller(numerator1: Int, denominator1: Int, numerator2: Int, denominator2: Int) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)

            assertThat(f1 < f2).isTrue()
        }

        @ParameterizedTest
        @CsvSource(
            "1, 3, 1, 4",
            "1, 5, 1, 5",
            "-2, 3, -4, 5"
        )
        fun `greater or equal`(numerator1: Int, denominator1: Int, numerator2: Int, denominator2: Int) {
            val f1 = Fraction(numerator1, denominator1)
            val f2 = Fraction(numerator2, denominator2)

            assertThat(f1 >= f2).isTrue()
        }
    }

}