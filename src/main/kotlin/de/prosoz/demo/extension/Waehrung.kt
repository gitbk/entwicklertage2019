package de.prosoz.demo.extension

data class Euro(val cent: Int)

val Int.euro: Euro get() = Euro(this)