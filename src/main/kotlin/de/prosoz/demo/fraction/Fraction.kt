package de.prosoz.demo.fraction

import kotlin.math.abs
import kotlin.math.sign

class Fraction(num: Long, denom: Long) : Comparable<Fraction> {
    private val gcd = gcd(abs(denom), abs(num))
    private val sign = num.sign * denom.sign

    val denominator = if (denom == 0L) throw IllegalArgumentException("Denominator is 0") else abs(denom) / gcd
    val numerator = sign * abs(num) / gcd

    private val value: Double
        get() = numerator.toDouble() / denominator.toDouble()

    constructor(num: Int, denom: Int) : this(num.toLong(), denom.toLong())
    constructor(number: Long) : this(number, 1L)
    constructor(number: Int) : this(number.toLong(), 1L)

    //<editor-fold desc="desctruction methods">
    operator fun component1() = numerator

    operator fun component2() = denominator
    //</editor-fold>

    //<editor-fold desc="operation methods">
    operator fun plus(other: Fraction) = Fraction(
        this.numerator * other.denominator + other.numerator * this.denominator,
        this.denominator * other.denominator
    )

    operator fun minus(other: Fraction) = Fraction(
        this.numerator * other.denominator - other.numerator * this.denominator,
        this.denominator * other.denominator
    )

    operator fun times(other: Fraction) = Fraction(
        this.numerator * other.numerator, this.denominator * other.denominator
    )

    operator fun div(other: Fraction) = Fraction(
        this.numerator * other.denominator, this.denominator * other.numerator
    )
    //</editor-fold>

    //<editor-fold desc="equals, hashCode, toString">
    override fun equals(other: Any?) = when {
        this === other -> true
        other == null -> false
        other !is Fraction -> false
        else -> this.numerator == other.numerator && this.denominator == other.denominator
    }

    override fun hashCode() = (numerator * denominator).toInt()

    override fun toString() = if (denominator == 1L || numerator == 0L) "$numerator" else "$numerator / $denominator"
    //</editor-fold>

    override fun compareTo(other: Fraction): Int = this.value.compareTo(other.value)

    companion object {
        private tailrec fun gcd(a: Long, b: Long): Long = if (b == 0L) a else gcd(b, a % b)

        fun parse(text: String): Fraction {
            fun parse(number: String, description: String) = try {
                number.toLong()
            } catch (ex: NumberFormatException) {
                throw IllegalArgumentException("$description is not a number")
            }

            val parts = text.split("/").map { it.trim() }
            if (parts.size == 1) return Fraction(parse(parts[0], "Numerator"))
            require(parts.size == 2) { "Can't find numerator and denominator" }
            val numerator = parse(parts[0], "Numerator")
            val denominator = parse(parts[1], "Denominator")
            return Fraction(numerator, denominator)
        }

        fun parseOrNull(text: String?): Fraction? {
            if (text == null) return null
            val parts = text.split("/").map { it.trim() }
            if (parts.size == 1) return parts[0].toLongOrNull()?.let { Fraction(it) }
            if (parts.size != 2) return null
            val numerator = parts[0].toLongOrNull() ?: return null
            val denominator = parts[1].toLongOrNull() ?: return null
            if (denominator == 0L) return null
            return Fraction(numerator, denominator)
        }
    }
}

//<editor-fold desc="Extension functions">
fun String.toFraction() = Fraction.parse(this)

fun String?.toFractionOrNull() = Fraction.parseOrNull(this)

fun Int.toFraction() = Fraction(this.toLong())
fun Long.toFraction() = Fraction(this)
//</editor-fold>
