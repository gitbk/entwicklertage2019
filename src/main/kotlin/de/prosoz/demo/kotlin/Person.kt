package de.prosoz.demo.kotlin

data class Person(val id: String, val vorname: String, val nachname: String)