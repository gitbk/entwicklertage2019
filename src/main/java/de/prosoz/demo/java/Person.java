package de.prosoz.demo.java;

import java.util.Objects;

import static java.util.Objects.nonNull;

public class Person {
    private String id;
    private String vorname;
    private String nachname;

    public Person() {
        id = "";
        vorname = "";
        nachname = "";
    }

    public Person(String id, String vorname, String nachname) {
        nonNull(id);
        nonNull(vorname);
        nonNull(nachname);

        this.id = id;
        this.vorname = vorname;
        this.nachname = nachname;
    }

    public String getId() {
        return id;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return id.equals(person.id) &&
                vorname.equals(person.vorname) &&
                nachname.equals(person.nachname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, vorname, nachname);
    }

    @Override
    public String toString() {
        return "Person(id = " + id + ", vorname = " + vorname + ", nachname = " + nachname +")";
    }
}
